**Guide to installation and first use**

[[_TOC_]]

# Runtime Environment

The python script is best cloned from the source (not downloaded) and executed within an Anaconda environment (not within your standard python environment). The first is usefull for uncomplicated updates and an easy contribution to the project, the later is ought to protect your habitual working environments and keep them clean. The installation and execution procedures are kept simple in order to allow users to analyze data who are not familiar to python.

## GIT Environment

*pyIOSSR* is a collaborative project, we would like to encourage that you get started with [git](https://git-scm.com/downloads) in order to join the developpers. Anyway, please use to following method to install the relevant scripts on your computer: 

1. make sure git is installed
  A. in Windows search for `gitbash` and install [git for windows](https://gitforwindows.org/) if you can not locate it, 
  B. in POSIX systems try `which git` and install [git](https://git-scm.com/downloads) if no answer is given to you; 
2. copy the project files from the internet onto your computer
  1. open *gitbash* or *terminal* respectively,
  2. navigate to a location that you want to use as working directory - e.g. in Windows `C:\working\directory` or in POSIX sustems `/working/directory`, 
  3. type `git clone https://srvgitlab.clpu.int/japinaniz/pyiossr.git` and download the project files by executing with `enter` - here `https://srvgitlab.clpu.int/japinaniz/pyiossr.git` is the default repository location which you can change if your institution has another version of the code on some server - or if you have a version of the code in a file;
3. if you open a filemanager, you can take a look in the created directory `pyiossr` - it should be populated with some files and directories.

## Python Environment

We recommend you to set up a clean python environment with [Anaconda](https://www.anaconda.com/distribution/). Set up Anaconda (version 3) by following the installation guides on the project's web page. If you do not like to work with Anaconda, please find the details of used packages in `pyridi/pyridi.yml`. Depending on the operating system, the next setup steps differ.

### Windows
Open Anaconda navigator, navigate to *Environments* and click on the play button behind *base* to launch the anaconda prompt. Navigate to the `pyiossr` directory, then execute the following installation instructions (answer with yes, if asked for permission to install packages)
```
(base) C:\working\directory\pyiossr> conda env create -f IOSSR.yml
(base) C:\working\directory\pyiossr> conda activate IOSSR
(IOSSR) C:\working\directory\pyiossr> 
```

### Linux
```
(base) user@computer:/working/directory/pyiossr$ conda env create -f IOSSR.yml
(base) user@computer:/working/directory/pyiossr$ conda activate IOSSR
(IOSSR) user@computer:/working/directory/pyiossr$
```

## Test Run

Perform a first run as test to ensure that the environment works fine. Execute
```
python run.py
```

## Known Errors and Troubleshooting

### OpenCV

Often the module *CV2* is not recognized, manifested with the error
```
    import cv2
ImportError: DLL load failed: The specified module could not be found.
```
then do
```
conda update -n base -c defaults conda
conda install -c fastai opencv-python-headless
conda uninstall opencv
conda uninstall opencv-python-headless
y
```
which shakes a bit the faulty installation and eventually brings you CV2 with GUI support.

---

# Workflow

Make sure you are working in an Anaconda prompt and the proper conda environment `IOSSR` is activated via `conda activate IOSSR`. Go to the directory that contains IOSSR's `run.py` and run via
```
python run.py
```

After some seconds a GUI interface pops up (it can take a while if you are using the script for the first time). To change the settings from default values klick on `SETTINGS`. Maximize the window for a better overview. Either you type in the input parameters manually, or you load them from a input file that was readied for your task. A help button guides you regarding the meaning of the parameters. If you are the first to set up input parameters, you may export them in a file via `EXPORT TO FILE`. Most important parameters are `disrel` and `pixel_per_mm`.

If you are not sure about the resolution of the image that needs to be put in the field `pixel_per_mm`, you may use the tool `SCR/dft_algorithm.py` via `python SCR/dft_algorithm.py`. Therin, you can select a well illuminated reference file, where a known scale close to the detector screen is visible. The script guides you via terminal prints.

After setting up the program, click `APPLY`. The main GUI pops up. In order to enable full functionality the program requires a reference for the coordinate zero, i.e. the intersection of the screen with the axis that passes through source and entrance pinhole of the spectromter. When using a Thomson Parabola a reference voltage is required, the applied voltage can differ from the effective voltage in the spectrometer. A click on `BROWSE REFERENCE` starts a sequence of reference prompts:

1. selection of a region of interest via cropping (in order to reduce the computational effort)
2. selection of the zero point via klick
3. adjustment of the actual voltage that acts on particles, and the trace width (for integration).

Now after completing the references most buttons in the GUI are enabled. Change the working directory to the place where the data is saved to, e.g. `C:\DATA_EXPERIMENT\spectrometer`. Change the output directory to the directory where analyzed data is stored, e.g. `\\srvlab\Campaigns\00XXX-V0X-00000-0000\ANALYSIS\spectrometer\output`.

You are ready to start. Open the image acquisition application and set up auto saving towards the working directory.

---

# Milestones

- &check; setup project repository with first version
- &check; create user dialogue for cropping of data and definition for the zero
- &cross; workover GUI
- &cross; design test run scenario

