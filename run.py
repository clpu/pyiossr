# -*- coding: utf-8 -*-
"""
USER INFORMATION TO BE FOUND IN THE FILE README.MD

project             pythonic ion ...
acronym             pyIOSSR
created on          2021-10-19:10:00

@author             Jon Imanol Apiñaniz (Jon), CLPU, Villamayor, Spain
@moderator          Micha (MEmx), CLPU, Villamayor, Spain
@updator            Micha (MEmx), CLPU, Villamayor, Spain
            
@contact            japinaniz@clpu.es

interpreter         python > 3.8
version control     git

requires explicitely {
  - opencv
  - tifffile
  - matplotlib
  - scipy
  - scikit-image
  - imutils
  - tkinter
}
  
which includes implicitely {
  - numpy
  - inspect
  - importlib
  - os
  - sys
  - time
}

input file {
  - CSV COLUMNS :: KEY, VALUE, HELP
  - CSV ROWS    :: ASSIGNMENTS {KEY => VALUE}
}

flags {
  - 
}

execute command: python run.py *args[-f input_file.csv]

"""
# ==============================================================================
# IMPORT PYTHON MODULES
# ==============================================================================
# EXTERNAL
from importlib import reload                    # !! management of self-made modules
from inspect import getsourcefile               # !! inquiry of script location

import os                                       # !! methods of operating system
import sys                                      # !! executions by operating system
import time                                     # !! time and date
from multiprocess import Process as multiProc   # !! async processes
from multiprocess import Queue as multiQue      # !! async queue
from multiprocess import Manager as multiMan    # !! async manager
from multiprocess import Value as multiVal      # !! async value

import numpy as np                              # !! relatively fast array operations
import cv2                                      # !! operations on images
import imutils                                  # !! math on images

import readchar                                 # !! allows to read key input of stdin

import tkinter
from tkinter import *
from tkinter import ttk
import tkinter as tk 

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backend_bases import key_press_handler
from matplotlib.backend_bases import MouseButton
from matplotlib.figure import Figure
import matplotlib.image as mpimg
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)

import PIL.Image, PIL.ImageTk

import random
from scipy import interpolate

import glob

# EXTEND PATH
root = os.path.dirname(os.path.abspath(getsourcefile(lambda:0))) # !! get environment
sys.path.append(os.path.abspath(root+os.path.sep+'LIB'))         # !! add library directory
sys.path.append(os.path.abspath(root+os.path.sep+'SCR'))         # !! add script directory

# INTERNAL
import constants as const       # !! global constants from LIB/constants.py
import functions as func        # !! global formats from SCR/functions.py
import loader as load           # !! routines fetching input data from SCR/loader.py
import exporter as export       # !! routines writing out data from SCR/exporter.py

import image as image            # !! treatment of graphical data from SCR/image.py

import dft_algorithm as spatialcal # !! treatment of spatial calibration data from SCR/dft_algorithm.py

# RELOAD
reload(const)
reload(func)
reload(load)
reload(export)
reload(spatialcal)
reload(image)

# STYLES
plt.style.use(root+os.path.sep+'LIB'+os.path.sep+'plots.mplstyle')

# ==============================================================================
# SET UP RUNTIME ENVIRONMENT
# ==============================================================================
# SET RUNTIME VARIABLE DICTIONARY
runtime_vars = {}

# COMMAND LINE INPUT
def tuple_type(strings):
    # from https://stackoverflow.com/a/67474062
    strings = strings.replace("(", "").replace(")", "")
    mapped_int = map(int, strings.split(","))
    return tuple(mapped_int)

import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-f','--filename', nargs='?', type=str, default=None, help='Filename of input file.')
parser.add_argument('-cnv','--canvas', nargs='?', type=tuple_type, default=None, help='Cropping coordinates (xc,yc,+w,+h)')
parser.add_argument('-zr','--zero', nargs='?', type=tuple_type, default=None, help='Zero point coordinates (x0,y0)')

parser.add_argument('-ix','--xinvert', nargs='?', type=bool, default=False, help='Invert loaded pictures with respect to x coordinates.')
parser.add_argument('-iy','--yinvert', nargs='?', type=bool, default=False, help='Invert loaded pictures with respect to y coordinates.')

runtime_vars['command'] = parser.parse_args()

del argparse, parser

# TAKE TIME
debut = time.time()

# PROMPT GREETINGS
print('\nWELCOME TO IOSSR')

# CLEARIFY RUN
script_path,work_path = func.ENVIRONMENT(root = root)

# SET PARAMETERS


# ROOM FOR TESTS
# ..

# ==============================================================================
# LOAD VARIABLES
# ==============================================================================
# PARSE INPUT FILES
if runtime_vars['command'] == None:
    input_dict = load.parse_inputfile(const.ns_inputfile,[script_path,work_path])
else:
    input_dict = load.parse_inputfile(runtime_vars['command'].filename,[script_path,work_path])

# ==============================================================================
# SET UP RUNTIME VARIABLES
# ==============================================================================
# GLOBALS
# ..

# ENVIRONMENT
a = root

# TEST CASE
c = root+os.path.sep+"TEST"+os.path.sep+"SHOT_2019-06-19-140050-0000.tif"
# !TODO test case is not in git versioncontrol

# GLOBAL VARIABLES
runtime_vars['tracefinder_variable'] = 0.
runtime_vars['plot_type'] = 'log'

# RUNTIME PARSER
runtime_vars['trace_width'] = input_dict['trace_width']
runtime_vars['bg_trace_width'] = input_dict['bg_trace_width']
runtime_vars['working_dir'] = func.GIVE_DIRNAME(input_dict['working_dir'])
runtime_vars['output_dir'] = func.GIVE_DIRNAME(input_dict['output_dir'])
runtime_vars['background_subtraction'] = input_dict['background_subtraction']

# ==============================================================================
# SET UP MODULES
# ==============================================================================
def load_image(*args,**kwargs):
    """
    LOAD IMAGE
    **kwargs:
    filename :: optional, dir of address
    m_GUI    :: optional, GUI if True, CMD else
    m_path   :: optional, base dir for file look-up,
                 important if m_GUI is set True
    m_canvas :: optional, cropping coordinates (x0,y0,w,h),
    """
    # PARSE GLOBALS
    global runtime_vars
    glob_dict = globals()
    # INQUIRE OPTIONAL ARGUMENTS
    # modes of operation with defaults
    m_GUI = kwargs.get('m_GUI', False)
    m_path = kwargs.get('m_path', None)
    m_canvas = kwargs.get('m_canvas', None)
    # exits query?
    filename = kwargs.get('filename', None)
    # SET UP VARIABLES
    feedback_message = None
    # GET FILE LOCATION
    if not isinstance(m_path, str):
        m_path = os.path.expanduser('~')
    if not isinstance(filename, str):
        if m_GUI == True:
            filename =  tk.filedialog.askopenfilename(\
                            initialdir = runtime_vars['working_dir'],\
                            title = "Select a File",\
                        )
        else:
            filename = input(" > type dir/filename.type and confirm with ENTER: ")
    # update working dir
    runtime_vars['working_dir'] = func.GIVE_DIRNAME(filename)
    # FEEDBACK
    func.MESSAGE_PRINT(glob_dict['__file__'],"target file is "+filename)
    # LOAD FILE
    if image.ISIMG(filename):
        img = image.IMREAD(filename)
    else:
        img = None
        tmp_message = "target file is not recognized as image"
        func.MESSAGE_PRINT(glob_dict['__file__'],tmp_message)
        feedback_message = tmp_message
    # JON'S COORDINATE CONVENTION
    if isinstance(img, np.ndarray):
        img = np.flipud(img) # flip from scren to plot coordinate system
        img = np.fliplr(img) # flip from back-side view to ion view
    # USER INPUT INVERSION
    if runtime_vars['command'].xinvert == True:
        img = np.fliplr(img)
    if runtime_vars['command'].yinvert == True:
        img = np.flipud(img)
    # CROPPING
    mode = 'RECTANGULAR'
    if isinstance(img, np.ndarray):
        if isinstance(m_canvas,tuple):
            (x0,y0,w,h) = m_canvas
            cropped = {'mode':mode,'corner_x':x0,'corner_y':y0,'width':w,'height':h}
        elif isinstance(m_canvas,dict):
            cropped = m_canvas
        else:
            if m_GUI == True:
                cropped = image.INTERACTIVE_CROP(img, adjust=True, mode=mode)
            else:
                x0 = input(" > type x coordinate for crop and confirm with ENTER: ")
                y0 = input(" > type y coordinate for crop and confirm with ENTER: ")
                w  = input(" > type width for crop and confirm with ENTER: ")
                h  = input(" > type height for crop and confirm with ENTER: ")
                cropped = {'mode':mode,'corner_x':x0,'corner_y':y0,'width':w,'height':h}
    else:
        cropped = None

    # RETURN
    return {'filename' : filename, 'image' : img, 'canvas' : cropped, 'message' : feedback_message}

# ==============================================================================
# SET UP FUNCTIONS
# ==============================================================================
# TRACE SIMULATION AS FUNCTION OF ENERGY AND VOLTAGE (NEW TP MODEL DISTANCES TAKEN INTO ACCOUNT)
def trace_simulation(en, volt):
    global input_dict
    global const
    return np.array([(input_dict['Z_eff']*const.q_e*0.3545*0.075*0.1345)/np.sqrt((2*input_dict['atomic_mass']*const.m_u*en*input_dict['Z_eff']*const.q_e*10**6)), (input_dict['Z_eff']*const.q_e*(volt/0.016)*0.075*0.1345)/(2*en*input_dict['Z_eff']*const.q_e*10**6)])

def derive_x_via_trace_simulation(tracefinder_variable,**kwargs):
    global const
    global input_dict
    global runtime_vars
    """
    SIMULATE X POSITION ACCORDING TO Y POSITION VIA trace_simulation
    returns 'xpixels': in runtime_vars as np.ndarray
            'x_of_y_tracefinder': in runtime_vars as x = f-1(y)
    """
    # UPDATES
    runtime_vars = kwargs.get('runtime_vars',runtime_vars)
    # TRACE SIMULATION
    # simulated trace creation to [[y],[x]] in units of [m]
    simtrace = trace_simulation(np.arange(0.3, 40, 0.1), tracefinder_variable) #!TODO simranges to global vars
    # transform trace to units of pixels
    simtrace_pix=np.round(simtrace*const.m_to_mm*runtime_vars['pixel_per_mm']).astype(int)
    # INTERPOLATION AND EXTRAPOLATION
    # interpolating functions that can be subject to change during runtime
    # .. position funtion, introducing the vertical pixel position gives you the horizontal position (of the trace)
    x_of_y_tracefinder=interpolate.interp1d(simtrace_pix[0], simtrace_pix[1], fill_value="extrapolate")
    # runtime_vars['x_of_y_tracefinder']  = x_of_y_tracefinder
    # .. derive x-positions of pixels according to y positions and the simulated trace
    # runtime_vars['xpixels']  = x_of_y_tracefinder(runtime_vars['ypixels'])
    # return
    return x_of_y_tracefinder
   
def imprint_trace(imagecrop,x_trace,y_trace,**kwargs):
    global const
    global input_dict
    global runtime_vars
    glob_dict = globals()
    # UPDATES
    runtime_vars = kwargs.get('runtime_vars',runtime_vars)
            
    # optionals
    trace_width = kwargs.get('trace_width',runtime_vars['trace_width'])
    linewidth = kwargs.get('linewidth',int(runtime_vars['bg_trace_width']))
    
    # frame trace by boundary
    trace_is_maxval = np.nanmax(imagecrop) #int(image.BITRANGE(imagecrop.dtype)[1] - 1)
    for iy,y in enumerate(y_trace):
        x = int(x_trace[iy])
        imagecrop[y, x-int(trace_width/2.)-linewidth : x-int(trace_width/2.)]=trace_is_maxval
        imagecrop[y, x+int(trace_width/2.) : x+int(trace_width/2.) + linewidth]=trace_is_maxval
        
    return imagecrop

# SUPERPOSE 2D ARRAY AS POSITIONS ON IMAGE
def superpose_2D_to_img(img,lst,greyval):
    '''
    TAKE 2D ARRAY AS MATRIX INDICES TO PLOT GREYVAL ONTO IMAGE
    '''
    img[lst] = greyval
    return img

# ==============================================================================
# SET UP GUI
# ============================================================================== 
def interactive_input(**kwargs):
    """
    GUI SETUP FOR INPUT PARAMETERS
    **kwargs:
    anyfile  :: optional, bool to get remote input or work with default
    filename :: optional, dir of input file, important if anyfile is set True
    prompts a window with a menue
    - save     :: save fields to input file
    - apply    :: go to run with info in fields
    """
    # GLOABALS I/O
    global input_dict
    global const
    
    # PARAMETERS
    def exit_function():
        return True
    exit_call = kwargs.get('exit_call',exit_function)
    
    # INQUIRE OPTIONAL ARGUMENTS
    # filename for display input
    anyfile  = kwargs.get('anyfile', False)
    filename = kwargs.get('filename', const.ns_inputfile)
    runtime_vars = kwargs.get('runtime_vars', {})
    
    # FIND RUNTIME ENVIRONMENT
    for key in runtime_vars.keys():
        if key in input_dict.keys() and key != 'disrel':
            input_dict[key] = runtime_vars[key]
    
    # GUI INI
    # initiate the input window 
    inputwin = tk.Tk()
    this_is_the_lastly_opened_input_window = True
    
    # set window title 
    inputwin.title("INPUT MENU")
       
    # set window size via trim("int(width) x int(height) + int(xposition) + int(yposition)")
    wxh = func.screensize()
    screen_width = int(wxh.split('x')[0])
    screen_height = int(wxh.split('x')[1])
    scale = 3
    geometry_string = str(int(screen_width/scale))+"x"+str(int(screen_height/scale))+\
                       "+"+str(int(screen_width/3))+"+"+str(int(screen_height/3))
    inputwin.geometry(geometry_string)
       
    # set window background
    inputwin.config(background = "black")
    
    # GUI SCROLLBAR
    # create main frame
    main_frame = Frame(inputwin)
    main_frame.pack(fill=BOTH, expand = 1)
    # create a canvas, the only native thing with a scrollbar
    main_canvas = Canvas(main_frame)
    main_canvas.pack(side=LEFT, fill=BOTH,expand=1)
    # ad a scrollbar to frame, relate it to canvas
    scrollbar = ttk.Scrollbar(main_frame, orient=VERTICAL, command=main_canvas.yview)
    scrollbar.pack(side=RIGHT, fill=Y)
    # configure the canvas
    main_canvas.configure(yscrollcommand = scrollbar.set, background = "black")
    main_canvas.bind('<Configure>', lambda e: main_canvas.configure(scrollregion = main_canvas.bbox("all")))
    # create frame inside canvas
    slave_frame = Frame(main_canvas)
    slave_frame.config(background = "black")
    # add frame to window in canvas
    main_canvas.create_window((0,0), window = slave_frame, anchor="nw")
    
    # GUI MAIN APP
    
    # manifest logo
    background_image = image.IMREAD(root+os.path.sep+"MEDIA"+os.path.sep+"IMG"+os.path.sep+"CLPU_SF.png")
    bg_width = background_image.shape[1]
    bg_height = background_image.shape[0]
    bg_proportion = 3
    
    bg_scale_width = bg_width / (screen_width/scale) * bg_proportion
    bg_scale_height = bg_height / (screen_height/scale) * bg_proportion
    bg_scale = int(max(bg_scale_height,bg_scale_width))+1 # .. '+1' as primitive floor operation
    background_image = background_image[::bg_scale,::bg_scale,:]
    
    background_image = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(background_image))
    canvas = tk.Canvas(slave_frame,width = bg_width/bg_scale, height = bg_height/bg_scale)
    canvas.create_image(0,0,image=background_image, anchor="nw")
    
    # create labels
    label_title =   Label(\
                      slave_frame,\
                      text = "IOSSR", \
                      font=('Arial 21 bold'),\
                      width = 6, height = 2,\
                      fg = "lightgreen", \
                      background="black"\
                    )
    label_coucou =  Label(\
                      slave_frame,\
                      text = "Adjustments to input parameters:", \
                      font=('Arial 12 bold'),\
                      width = 30, height = 2,\
                      fg = "lightgreen", \
                      background="black"\
                    )
                    
    # create input fields
    rownumber = 6
    field_dict = {}
    for key in input_dict.keys():
        if 'help' not in key:
            # label for row
            Label(slave_frame,\
              text=key+' :: ',\
              font=('Arial 10 bold'),\
              fg = "lightgreen", \
              background="black"\
            ).grid(column=1, row=rownumber)
            # input field for row
            exec("field_dict['"+key+"_value'] = tk.StringVar(slave_frame, name=key, value=str(input_dict['"+key+"']))")
            exec("field_dict['"+key+"_entry'] = tk.Entry(slave_frame,"+\
                                    "textvariable= field_dict['"+key+"_value'],"+\
                                    "font=('Arial 10')"+\
                                  ").grid(column=2, row=rownumber, sticky='news')")
            # button for row
            exec("Button(slave_frame,"+\
              "text='Help', "+\
              "font=('Arial 10'),"+\
              "command=lambda: tk.messagebox.showwarning('Help', str(input_dict['"+key+"_help']))"+\
            ").grid(column=3, row=rownumber)")
        
            # Close row
            rownumber = rownumber + 1
    
    # control option
    #for key in input_dict.keys():
    #    exec("print("+key+"_value.get())")
    
    # helper function: get path
    def get_path(title,**kwargs):
        global input_dict
        io = kwargs.get('io',None)
        if io == 'save':
            return tk.filedialog.asksaveasfilename(\
                            initialdir = runtime_vars['working_dir'],\
                            title = title,\
                            defaultextension=".txt"\
                        )
        elif io=='open':
            return tk.filedialog.askopenfilename(\
                            initialdir = runtime_vars['working_dir'],\
                            title = title,\
                            defaultextension=".txt"\
                        )
        else:
            return tk.simpledialog.askstring(\
                            title = title,\
                            prompt="path/to/dir/file.csv"
                        )
   
    # helper function: reload window with new input parameters
    def rewind_input_dict(file):
        global input_dict
        nonlocal this_is_the_lastly_opened_input_window
        input_dict = load.parse_inputfile(file,[])
        inputwin.destroy()
        this_is_the_lastly_opened_input_window = False
        interactive_input(runtime_vars = runtime_vars)
        return True
        
    # helper function: prepare and execute export of settings
    def execute_export(file,field_dict,input_dict):
        export.tkinter_to_inputfile(file,field_dict,input_dict)
        return True
    
    # create buttons
    button_load_any      =   Button(\
                               slave_frame,  \
                               text = "IMPORT PARAMETERS", \
                               font=('Arial 12 bold'),\
                               command = lambda: rewind_input_dict(get_path("Select source file",io="open"))\
                             )
    
    button_goto_save     =   Button(\
                               slave_frame,  \
                               text = "EXPORT TO FILE", \
                               font=('Arial 12 bold'),\
                               command = lambda: execute_export(get_path("Save as file",io="save"),field_dict,input_dict)\
                             )
    button_goto_main     =   Button(\
                               slave_frame,  \
                               text = "APPLY", \
                               font=('Arial 12 bold'),\
                               command = inputwin.destroy\
                             )  
    
    # positioning
    canvas.grid(column = 1, row = 1)
    label_title.grid(column = 2, row = 1)
    label_coucou.grid(column = 2, row = 2)
    
    button_load_any.grid(column =2, row= 3, sticky="news")
    button_goto_save.grid(column =2, row= 4, sticky="news")
    button_goto_main.grid(column =2, row= 5, sticky="news")
    
    # TODO add scrollbar
    
    # create window
    inputwin.mainloop()
    
    # read fields and parse them to dictionary
    for key in input_dict.keys():
        if 'help' not in key and this_is_the_lastly_opened_input_window:
            try:
                exec("input_dict['"+key+"'] = float(field_dict['"+key+"_value'].get())")
            except:
                exec("input_dict['"+key+"'] = field_dict['"+key+"_value'].get()")
    # write working directory in runtime parameters
    runtime_vars['trace_width'] = input_dict['trace_width']
    runtime_vars['bg_trace_width'] = input_dict['bg_trace_width']
    runtime_vars['working_dir'] = func.GIVE_DIRNAME(input_dict['working_dir'])
    runtime_vars['output_dir'] = func.GIVE_DIRNAME(input_dict['output_dir'])
    runtime_vars['background_subtraction'] = input_dict['background_subtraction']
    # exit
    return exit_call()

def interactive_start():
    """
    GUI FOR START DIALOGUE
    prompts a window with a menue
    - start    :: continue to the main programme
    - settings :: go to change of default settings
    """
    # globals 
    global root

    # set output flags
    goto = None
    # initiate the start-up window 
    startup = tk.Tk()
    
    # set window title 
    startup.title("START MENU")
       
    # set window size via trim("int(width) x int(height) + int(xposition) + int(yposition)")
    wxh = func.screensize()
    screen_width = int(wxh.split('x')[0])
    screen_height = int(wxh.split('x')[1])
    scale = 3
    geometry_string = str(int(screen_width/scale))+"x"+str(int(screen_height/scale))+\
                       "+"+str(int(screen_width/3))+"+"+str(int(screen_height/3))
    startup.geometry(geometry_string)
       
    # set window background
    startup.config(background = "black")
    
    # manifest logo
    background_image = image.IMREAD(root+os.path.sep+"MEDIA"+os.path.sep+"IMG"+os.path.sep+"CLPU_SF.png")
    bg_width = background_image.shape[1]
    bg_height = background_image.shape[0]
    bg_proportion = 3
    
    bg_scale_width = bg_width / (screen_width/scale) * bg_proportion
    bg_scale_height = bg_height / (screen_height/scale) * bg_proportion
    bg_scale = int(max(bg_scale_height,bg_scale_width))+1 # .. '+1' as primitive floor operation
    background_image = background_image[::bg_scale,::bg_scale,:]
    
    background_image = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(background_image))
    canvas = tk.Canvas(startup,width = bg_width/bg_scale, height = bg_height/bg_scale)
    canvas.create_image(0,0,image=background_image, anchor="nw")
       
    # create labels
    label_title =   Label(\
                      startup,\
                      text = "IOSSR", \
                      font=('Arial 21 bold'),\
                      width = 6, height = 2,\
                      fg = "lightgreen", \
                      background="black"\
                    )
    label_coucou =  Label(\
                      startup,\
                      text = "The online analysis for TP toolbox!", \
                      font=('Arial 12 bold'),\
                      width = 30, height = 2,\
                      fg = "lightgreen", \
                      background="black"\
                    )

    # create buttons     
    def goto_settings():
        startup.destroy()
        interactive_input(runtime_vars = runtime_vars)
        return True
    
    button_goto_settings =   Button(\
                               startup,  \
                               text = "SETTINGS", \
                               font=('Arial 12 bold'),\
                               command = goto_settings\
                             )
     
    def goto_main():
        startup.destroy()
        interactive_runtime()
        return True
    
    button_goto_main     =   Button(\
                               startup,  \
                               text = "START", \
                               font=('Arial 12 bold'),\
                               command = goto_main\
                             )
    
    button_close_window  =   Button(\
                               startup,  \
                               text = "CLOSE", \
                               font=('Arial 12 bold'),\
                               command = lambda: exit()\
                             )  
    
    # positioning
    #canvas.place(x=0, y=0, relwidth=1, relheight=1)
    canvas.grid(column = 1, row = 1)
    label_title.grid(column = 2, row = 1)
    label_coucou.grid(column = 2, row = 2)

    button_goto_settings.grid(column =2, row= 3, sticky="news")
    button_goto_main.grid(column =2, row= 4, sticky="news")
    button_close_window.grid(column =2, row= 5, sticky="news")
    
    
    # create window
    startup.mainloop()

    return True
    
def interactive_runtime(*args,**kwargs):
    # here, doing nothing yields the goal
    return True
    
def interactive_tracefinder(img,startval,startval_trace,**kwargs):
    """
    GUI FOR INTERACTIVE PARABOLA TRACE DELIMITATION
    parameters
    IMG                 :: input image that holds a parabola
    STARTVAL            :: start value of the parabola tracer routine
    STARTVAL_TRACE      :: start value of the voltage tracer routine
    prompts a window with a menue
    - set               :: parse value in input field to trace plotter
    - apply             :: save value and output
    """
    # globals
    global root
    # optional parameters
    runtime_vars = kwargs.get('runtime_vars',{})
    # initiate standard output
    setvalue = startval
    setvalue_trace = startval_trace
    # initiate the start-up window 
    #  must be Toplevel as top level window if img-in-canvas-display
    tracefind = tk.Toplevel()
    
    # set window title 
    tracefind.title("TRACEFINDER")
       
    # set window size via trim("int(width) x int(height) + int(xposition) + int(yposition)")
    wxh = func.screensize()
    screen_width = int(wxh.split('x')[0])
    screen_height = int(wxh.split('x')[1])
    scale = 3
    geometry_string = str(int(screen_width/scale))+"x"+str(int(screen_height/scale))+\
                       "+"+str(int(screen_width/3))+"+"+str(int(screen_height/3))
    tracefind.geometry(geometry_string)
       
    # set window background
    tracefind.config(background = "black")
    
    # manifest logo
    background_image = image.IMREAD(root+os.path.sep+"MEDIA"+os.path.sep+"IMG"+os.path.sep+"CLPU_SF.png")
    bg_width = background_image.shape[1]
    bg_height = background_image.shape[0]
    bg_proportion = 3
    
    bg_scale_width = bg_width / (screen_width/scale) * bg_proportion
    bg_scale_height = bg_height / (screen_height/scale) * bg_proportion
    bg_scale = int(max(bg_scale_height,bg_scale_width))+1 # .. '+1' as primitive floor operation
    background_image = background_image[::bg_scale,::bg_scale,:]
    
    background_image = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(background_image))
    canvas = tk.Canvas(tracefind,width = bg_width/bg_scale, height = bg_height/bg_scale)
    canvas.create_image(0,0,image=background_image, anchor="nw")
       
    # create labels
    label_title =   Label(\
                      tracefind,\
                      text = "IOSSR", \
                      font=('Arial 21 bold'),\
                      width = 6, height = 2,\
                      fg = "lightgreen", \
                      background="black"\
                    )
    label_coucou =  Label(\
                      tracefind,\
                      text = "Adjustments to trace parameter:", \
                      font=('Arial 12 bold'),\
                      width = 30, height = 2,\
                      fg = "lightgreen", \
                      background="black"\
                    )
    # create input variable field for voltage
    value = tk.StringVar(tracefind, name='voltage', value=str(setvalue))
    entry_voltage = tk.Entry(tracefind,\
                     textvariable=value,\
                     font=('Arial 10')\
                    )
                    
    # create input variable field for trace width
    value_trace = tk.StringVar(tracefind, name='tracewidth', value=str(setvalue_trace))
    entry_trace = tk.Entry(tracefind,\
                     textvariable=value_trace,\
                     font=('Arial 10')\
                    )
    
    # create display canvas
    img_height,img_width = img.shape
    display_height = int(0.75*screen_height)
    display_width = int(display_height * img_width/img_height)
    display_scale = int(img_height/display_height)+1
    #display = tk.Canvas(tracefind,width = display_width, height = display_height, bg='black')
    #img_display_ini = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(image.ENHANCED_VISIBILITY(img[::display_scale,::display_scale])))
    #image_container = display.create_image(0,0,image=img_display_ini, anchor="nw")

    # create interactive matplotlib canvas
    plt.ion()
    dpi = 100
    fig = plt.figure(figsize=(display_width/dpi,display_height/dpi), dpi=dpi)
    ax = fig.add_subplot(111)
    ax.imshow(img[::display_scale,::display_scale])
    plt.tight_layout()
    plt.draw()
    
    # labels for information
    label_voltage = Label(tracefind,
                      text=("VOLTAGE:"),
                      width = 10, height = 1,  
                      fg="red",
                      background="black")
    label_trace =   Label(tracefind,
                      text=("TRACE WIDTH:"),
                      width = 10, height = 1,  
                      fg="red",
                      background="black")
    
    # draw on canvas     
    def plot_trace_and_image(img):
        # ARANGE PARAMETERS AND VARIABLES
        # access value in input field and variable
        nonlocal value
        nonlocal value_trace
        nonlocal setvalue
        nonlocal setvalue_trace
        #nonlocal image_container
        nonlocal ax
        # transfer value of input field to variable
        setvalue = value.get()
        setvalue_trace = value_trace.get()
        # USE VARIABLE TO PLOT
        # update xpixels and x_of_y_tracefinder
        # derive_x_via_trace_simulation(float(setvalue))
        print(runtime_vars["center"])
        x_of_y_tracefinder = derive_x_via_trace_simulation(float(setvalue))
        x_relevant = x_of_y_tracefinder(runtime_vars['ypixels']) + runtime_vars["center"][0]
        # load y from runtime_vars
        y_relevant = runtime_vars["ypixels_canvas"]
        # prepare image
        img_modi = imprint_trace(img.copy(),x_relevant,y_relevant,linewidth=min(display_scale,runtime_vars['bg_trace_width']), trace_width=float(setvalue_trace), runtime_vars=runtime_vars)
        # display
        #img_display = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(image.ENHANCED_VISIBILITY(img[::display_scale,::display_scale])))
        
        ax.clear()
        ax.imshow(img_modi[::display_scale,::display_scale])
        plt.tight_layout()
        plt.draw()

        # WRITE OUT
        #display.itemconfig(image_container,image=img_display)
        return True
    
    button_set =            Button(\
                               tracefind,  \
                               text = "SET", \
                               font=('Arial 12 bold'),\
                               command = lambda: plot_trace_and_image(img)\
                             )
                             
    # background subtraction along wings on/off
    background_subtraction = runtime_vars['background_subtraction']
    if background_subtraction == 'T' :
        bg_text = "SWITCH REMOVE BG OFF"
    else:
        bg_text = "SWITCH REMOVE BG ON"
        
    def bg_switch():
        nonlocal bg_text
        nonlocal background_subtraction
        if 'ON' in button_bg['text']:
            button_bg['text'] = button_bg['text'].replace('ON','OFF')
            background_subtraction = 'F'
        else:
            button_bg['text'] = button_bg['text'].replace('OFF','ON')
            background_subtraction = 'T'
        return True

    button_bg =            Button(\
                               tracefind,  \
                               text = bg_text, \
                               font=('Arial 12 bold'),\
                               command = bg_switch\
                             )
    
    # exit mainloop with grace
    def goto_main():
        nonlocal setvalue
        nonlocal setvalue_trace
        nonlocal fig
        # transfer value of input field to variable
        print(" > finder go byebye")
        setvalue = value.get()
        # transform into float variable
        setvalue = float(setvalue)
        setvalue_trace = float(setvalue_trace)
        # close plot and reset matplotlib
        plt.close("all")
        plt.ioff()
        del fig
        # destroy window
        tracefind.quit()
        tracefind.destroy()
    
    button_goto_main     =   Button(\
                               tracefind,  \
                               text = "APPLY AND EXIT", \
                               font=('Arial 12 bold'),\
                               command = lambda: goto_main()\
                             )
    
    # positioning
    canvas.grid(column = 1, row = 1)
    
    label_title.grid(column = 2, row = 1, columnspan = 2)
    label_coucou.grid(column = 2, row = 2, columnspan = 2)
    
    label_voltage.grid(column = 2, row = 3)
    label_trace.grid(column = 3, row = 3)
    
    entry_voltage.grid(column=2, row=4, sticky='news')
    entry_trace.grid(column=3, row=4, sticky='news')

    button_set.grid(column =2, row= 5, sticky="news", columnspan = 2)
    button_bg.grid(column =2, row= 6, sticky="news", columnspan = 2)
    button_goto_main.grid(column =2, row= 7, sticky="news", columnspan = 2)
    
    # create window
    tracefind.mainloop()

    # WRITE OUT
    return setvalue, setvalue_trace, background_subtraction
    
def select_batch(**kwargs):
    """
    DIALOGUE FOR INPUT FILE SELECTION WITH FILE DIALOGUE
    **kwargs:
    m_GUI      :: use GUI or terminal input
    prompts a window with a menue
    - save     :: save fields to input file
    - apply    :: go to run with info in fields
    """
    # GLOABALS I/O
    # none
    
    # INQUIRE OPTIONAL ARGUMENTS
    m_GUI  = kwargs.get('m_GUI', False)
    
    # DIALOGUE INIT
    if not m_GUI:
        filenames = input(" > type dir/filename.type dir/filename2.type and confirm with ENTER: ")
        filenames = filenames.split(' ')
    else:
        filenames = list(tk.filedialog.askopenfilenames(title='Select Batch'))
    
    # OUT
    return filenames


# ==============================================================================
# START RUN
# ==============================================================================
# MAIN RUNTIME FUNCTIONS
# browse reference file and load reference parameters
def explorador_referencia():
    # GLOBALS
    global runtime_vars
    glob_dict = globals()
    # RUN
    try:
        if isinstance(runtime_vars['command'].canvas,tuple):
            set_canvas = runtime_vars['command'].canvas
            print("   take canvas from command line")
        else:
            set_canvas = None
    except:
        set_canvas = None
    # masked image
    reference_image = load_image(m_GUI=True, m_canvas=set_canvas)
    runtime_vars['canvas'] = reference_image['canvas']
    # proceed if valid selection is given
    if runtime_vars['canvas'] != None:
        # center coordinates
        load_zero = False
        try:
            if isinstance(runtime_vars['command'].zero,tuple):
                runtime_vars['center'] = runtime_vars['command'].zero
                print("   take rel-pos zero from command line")
            else:
                load_zero = True
        except:
            load_zero = True
        if load_zero: runtime_vars['center'] = image.INTERACTIVE_POINT(image.CROP_TO_CANVAS(reference_image["image"],reference_image['canvas']),adjust=True)
        del load_zero
        runtime_vars['center'] = np.array(runtime_vars['center'])
        
        func.MESSAGE_PRINT(glob_dict['__file__'],"centre in cropped frame selected to be "+str(runtime_vars['center']))
        
        #creating cutoff/shot list !TODO
        #runtime_vars['cutoffs']=np.array([[0, 0]])
        
        # build y coordinates of trace on canvas
        print(" > derive y via calculation")
        calc_min_y_pixel = runtime_vars['pixstart'] + runtime_vars['center'][1]
        find_max_y_pixel = min(runtime_vars['pixend'] + runtime_vars['center'][1],runtime_vars['canvas']['height'])
        runtime_vars['ypixels_canvas'] = range(calc_min_y_pixel, find_max_y_pixel)

        # trace finder
        imagecrop = image.CROP_TO_CANVAS(reference_image['image'],reference_image['canvas'])
        runtime_vars['tracefinder_variable'],runtime_vars['trace_width'],runtime_vars['background_subtraction'] = interactive_tracefinder(imagecrop,runtime_vars['tracefinder_variable'],runtime_vars['trace_width'],runtime_vars = runtime_vars)
        
        # load x(runtime_vars['ypixels']) values to runtime_vars['xpixels'] and
        #  the trace in function of y positions to runtime_vars['x_of_y_tracefinder']
        #  with runtime_vars['ypixels'] from initial startup procedure
        print(" > derive x(y) via trace simulation")
        runtime_vars['x_of_y_tracefinder'] = derive_x_via_trace_simulation(runtime_vars['tracefinder_variable'])
        runtime_vars['xpixels'] = runtime_vars['x_of_y_tracefinder'](runtime_vars['ypixels'])
        runtime_vars['xpixels_canvas'] = runtime_vars['xpixels'] + runtime_vars['center'][0]

        # BEGIN LANEXPROT 
        #  MAYBE FOR FUTURE TODO
        #loading and formatting lanexprot
        #lanexprot is formatted as two column (vertical//horizantal)
        #lanexprot=np.genfromtxt(input_dict['lanex'])
        #lanexprot=np.stack((-lanexprot.T[0], lanexprot.T[1]),axis=1)
        #delete the two zero initial values
        #lanexprot=np.array([np.delete(lanexprot.T[0],0), np.delete(lanexprot.T[1],0)]).T
        #lanexprot=np.array([np.delete(lanexprot.T[0],0), np.delete(lanexprot.T[1],0)]).T
        #converting lanexprot into pixel units
        #lanexprot=np.round(lanexprot*const.m_to_mm*runtime_vars['pixel_per_mm']).astype(int)
        #centering in xray spot
        #lanexprot=lanexprot-runtime_vars['center']
        #inverting columns
        #runtime_vars['lanexprot']=np.stack((lanexprot.T[1], lanexprot.T[0]), axis=1)
        # END LANEXPROT

        return True
    else:
        return False

# browse data file, get data, parse data to physics, save results, display analysis results, save display and allow interction
def explorador_archivos(**kwargs): 
    # GLOBALS
    global a
    global root
    global input_dict
    global spectrum
   
    #global empty_Tk
    glob_dict = globals()
    
    # parameters
    new_plt_id = False

    # search for defaults
    runtime_vars = kwargs.get('runtime_vars',{})
    data_file = kwargs.get('data_file',None)
    silent_run = kwargs.get('silent_run',False)
    noplot_run = kwargs.get('noplot_run',False)
    matplotlib_id = kwargs.get('plt_ID',None)
    
    # high level defaults
    matplotlib_type = kwargs.get('plt_type',runtime_vars['plot_type'])
    
    # RUN
    print("\nNEW INPUT STREAM")
    if data_file is None:
        input_stream = load_image(m_GUI=True,m_canvas=runtime_vars['canvas'])
    else:
        input_stream = load_image(filename=data_file,m_GUI=True,m_canvas=runtime_vars['canvas'])
    #print(input_stream['message'])
    
    # image
    print(" > crop image")
    imagecrop = image.CROP_TO_CANVAS(input_stream['image'],input_stream['canvas'])
    
    # print trace on image
    print(" > imprint trace on cropped image")
    imagecrop_mod = imprint_trace(imagecrop.copy(),runtime_vars['xpixels_canvas'],runtime_vars['ypixels_canvas'], runtime_vars = runtime_vars)
      
    #creating signal list (for every value of lanexprot and runtime_vars['disrel_pixed'])
    #for every vertical pixel position, a number is created as a sum of horizontal pixels from "ini" to "fin", which should be
    #centered in the trace
    print(" > BG handler")
    signal_raw=np.array([0])
    background=np.array([0])
    for iy,y in enumerate(runtime_vars['ypixels_canvas']):
        x_of_y_and_shifted = runtime_vars['xpixels_canvas'][iy]
        ini = min( max(0, int(x_of_y_and_shifted - runtime_vars['trace_width']/2.)),   input_stream['canvas']['width'])
        fin = min( ini + int(runtime_vars['trace_width']),                             input_stream['canvas']['width'])
        print(x_of_y_and_shifted)
        print(ini)
        print(fin)
        signal_slice = np.sum(imagecrop[y,ini:fin])
        signal_raw = np.append(signal_raw, signal_slice)
        if ini < runtime_vars['bg_trace_width']:
            runtime_vars['bg_trace_width'] = ini
        if input_stream['canvas']['width'] - fin < runtime_vars['bg_trace_width']:
            runtime_vars['bg_trace_width'] = input_stream['canvas']['width'] - fin
        background_slice = ( np.sum(imagecrop[y,ini-int(runtime_vars['bg_trace_width']):ini]) + \
                             np.sum(imagecrop[y,fin:fin+int(runtime_vars['bg_trace_width'])]) ) \
                           / (2.*runtime_vars['bg_trace_width']) \
                           * runtime_vars["trace_width"]
        background = np.append(background, background_slice)
        print("DEBUG BG IT "+str(iy)+" Y "+str(y))
        print("x_of_y_and_shifted "+str(x_of_y_and_shifted))
        print("ini "+str(ini))
        print("fin "+str(fin))
        print("signal_slice "+str(signal_slice))
        print("background_slice "+str(background_slice))
        print("lower "+str(ini-int(runtime_vars['bg_trace_width'])))
        print("upper "+str(fin+int(runtime_vars['bg_trace_width'])))
        
    signal_raw=np.delete(signal_raw, 0)
    background=np.delete(background, 0)
    
    # background correction
    good_background = np.where(background<signal_raw,background,signal_raw)
    if runtime_vars['background_subtraction'] == 'T':
        signal = signal_raw - good_background
        print("   background substracted")
    else:
        signal=signal_raw
        print("   background not substracted")
        # !TODO raise warning
    
    #creating spectrum
    print(" > correct signal and build spectrum")
    correctedsignal=np.multiply(signal, runtime_vars['dxde_of_energy'](runtime_vars['energies'])[0:signal.shape[0]]) * const.m_to_mm*runtime_vars['pixel_per_mm']
    spectrum=np.stack((runtime_vars['energies'][0:signal.shape[0]], correctedsignal), axis=1)
    
    # writing out to file
    print(" > save spectrum and meta data")
    try:
        np.savetxt(runtime_vars['output_dir']+os.path.sep+func.STRIP_EXTENSION(func.GIVE_FILENAME(input_stream['filename']))+'_IOSSR_SPECTRUM.dat', spectrum, header='Energy/[MeV],Counts/MeV')
    except:
        print(" > attempt to save spectrum in output directory failed")
        
    try:
        export.dict_to_inputfile(runtime_vars['output_dir']+os.path.sep+func.STRIP_EXTENSION(func.GIVE_FILENAME(input_stream['filename']))+'_IOSSR_RUNTIME.dat',runtime_vars)
    except:
        print(" > attempt to save runtime data in output directory failed")
    
    # abort if silent
    if noplot_run:
        return True
    
    # plotter section
    print("\nPLOTTER")
    try:
        
        if plt.fignum_exists(runtime_vars['matplotlib'][matplotlib_id]['num']):
            # Figure is still opened
            fig = runtime_vars['matplotlib'][matplotlib_id]['fig']
            ax0,ax1,ax2 = runtime_vars['matplotlib'][matplotlib_id]['axs']
            ax0.clear()
            ax1.clear()
            ax2.clear()
            print(' > use plot with ID' + str(matplotlib_id))
            new_plt_id = False
            plt_ID = matplotlib_id
            
            # activate plt on this figure
            plt.figure(fig.number)
        
        else:
            # Figure is closed
            raise ValueError('Figure found closed.')
            
    except:
        
        if not silent_run:
            plt.ion()
            
        fig = plt.figure(figsize=(15,10), dpi=100)
        gs = fig.add_gridspec(2,2) #(3,2)
        ax0 = fig.add_subplot(gs[:, 0])
        ax1 = fig.add_subplot(gs[0, 1])
        ax2 = fig.add_subplot(gs[1, 1])
        
        plt_num = fig.number
        plt_ID = id(fig)
        new_plt_id = True
        if 'matplotlib' in runtime_vars.keys():
            runtime_vars['matplotlib'][plt_ID] = {}
        else:
            runtime_vars['matplotlib'] = {}
            runtime_vars['matplotlib'][plt_ID] = {}
        runtime_vars['matplotlib'][plt_ID]['fig'] = fig
        runtime_vars['matplotlib'][plt_ID]['axs'] = ax0,ax1,ax2
        runtime_vars['matplotlib'][plt_ID]['num'] = plt_num
        
        print(' > new plot with ID ' + str(plt_ID))
    
    # feedback
    print(' > sit on plot number '+str(fig.number))
    
    # general elements
    plt.title(func.STRIP_EXTENSION(func.GIVE_FILENAME(input_stream['filename'])) + ' ' + str(time.time()))
    
    # .. plots in function calls for later onclick conpatibility
    def plot_ax0():
        nonlocal ax0
        ax0.imshow(np.log(imagecrop_mod),cmap='plasma')
        ax0.set_title('Trace')
    plot_ax0()

    def plot_ax1():
        nonlocal ax1
        ax1.plot(spectrum.T[0], spectrum.T[1])
        ax1.set_xlabel('Energy (MeV)')
        ax1.set_ylabel('Imaged MCP Counts (/MeV)')
        ax1.set_yscale(matplotlib_type)
    plot_ax1()
    
    def plot_ax2():
        nonlocal ax2
        ax2.plot(spectrum.T[0],signal, label="corrected signal")
        ax2.plot(spectrum.T[0],signal_raw, label="recorded signal")
        ax2.plot(spectrum.T[0],background, label="interpolated background")
        ax2.set_xlabel('Energy (MeV)')
        ax2.set_ylabel('RAW Signal (/pixel)')
        ax2.set_yscale(matplotlib_type)
        ax2.legend()
    plot_ax2()
    
    #ax3.plot(runtime_vars['cutoffs'].T[0], runtime_vars['cutoffs'].T[1])
    #ax3.set_xlabel('Shot')
    #ax3.set_ylabel('Cut-off Energy (MeV)')
    
    # .. style event management
    plt.tight_layout()
    def resize(*args,**kwargs):
        plt.tight_layout()
    if not silent_run:    
        cre = fig.canvas.mpl_connect('resize_event', resize)
    
    # .. mouse klick event management
    coordinates = {'left_button':[None,None], 'right_button':[None,None]}
    energy_coor = {'left_button':None, 'right_button':None}
    line_color  = {'left_button':['darkorange','orangered'], 'right_button':['lightskyblue','deepskyblue']}
    def onclick(event):
        nonlocal coordinates
        nonlocal ax0
        nonlocal ax1
        nonlocal ax2
        if event.dblclick:
            if event.button is MouseButton.LEFT:
                coordinates['left_button'] = [event.xdata, event.ydata]   
                if event.inaxes is not None:
                    if id(event.inaxes) == id(ax0):
                            energy_coor['left_button'] = energy_of_y(event.ydata - runtime_vars['center'][1])
                    else:
                        energy_coor['left_button'] = event.xdata
            elif event.button is MouseButton.RIGHT:
                coordinates['right_button'] = [event.xdata, event.ydata]
                if event.inaxes is not None:
                    if id(event.inaxes) == id(ax0):
                            energy_coor['right_button'] = energy_of_y(event.ydata - runtime_vars['center'][1])
                    else:
                        energy_coor['right_button'] = event.xdata
            print(" > click coordinates "+str(coordinates))
            print(" > click energy "+str(energy_coor))
            if event.inaxes is not None:
                axi = event.inaxes
                if id(axi) == id(ax0):
                    # reset subplot ax0
                    ax0.clear()
                    plot_ax0()
                    # replot others
                    ax1.clear()
                    plot_ax1()
                    ax2.clear()
                    plot_ax2()
                    # left and right events
                    for button in coordinates.keys():
                        if coordinates[button][0] != None:
                            # find energy value
                            x_for_line = energy_coor[button]
                            # plot lines to ax1 and ax2
                            ax1.axvline(x=x_for_line,color=line_color[button][1], linestyle="--")
                            ax2.axvline(x=x_for_line,color=line_color[button][1], linestyle="--")
                            # plot croshair in active axis
                            y_for_line = y_of_energy(energy_coor[button]) + runtime_vars['center'][1]
                            ax0.axhline(y=y_for_line,color=line_color[button][0], linestyle="--")
                            ax0.text(runtime_vars['center'][0]+40,y_for_line-30,str(np.around(x_for_line,2))+" MeV",backgroundcolor="white")            
                elif id(axi) == id(ax1):
                    # reset subplot ax1
                    ax1.clear()
                    plot_ax1()
                    # replot others
                    ax2.clear()
                    plot_ax2()
                    # plot respective line in ax2 which has same x-coordinate
                    for button in coordinates.keys():
                        if coordinates[button][0] != None:
                            ax2.axvline(x=energy_coor[button],color=line_color[button][1], linestyle="--")
                elif id(axi) == id(ax2):
                    # reset subplot ax2
                    ax2.clear()
                    plot_ax2() 
                    # replot others
                    ax1.clear()
                    plot_ax1()
                    # plot respective line in ax1 which has same x-coordinate
                    for button in coordinates.keys():
                        if coordinates[button][0] != None:
                            ax1.axvline(x=energy_coor[button],color=line_color[button][1], linestyle="--")
                if id(axi) == id(ax2) or id(axi) == id(ax1):
                    # reset subplot ax0
                    ax0.clear()
                    plot_ax0()
                    # plot respective line in ax0 after evaluation
                    for button in coordinates.keys():
                        if coordinates[button][0] != None:
                            y_for_line = y_of_energy(energy_coor[button]) + runtime_vars['center'][1]
                            ax0.axhline(y=y_for_line,color=line_color[button][1], linestyle="--")
                            ax0.text(runtime_vars['center'][0]+40,y_for_line-30,str(np.around(coordinates[button][0],2))+" MeV",backgroundcolor="white")
                    # plot croshair in active axis
                    for button in coordinates.keys():
                        if coordinates[button][0] != None:
                            #axi.axhline(y=coordinates[button][1],color=line_color[button][0], linestyle="--")
                            axi.axvline(x=energy_coor[button],color=line_color[button][0], linestyle="--")
                #plt.figure(runtime_vars['matplotlib'][plt_ID]['num'])
                plt.draw()
                print(" > modify plot, reset with double click")
        return None
    if not silent_run:    
        cid = fig.canvas.mpl_connect('button_press_event', onclick)
    # .. show drawing
    if (matplotlib_id is None or new_plt_id) and not silent_run:
        print(" > show plot")
        #plt.figure(runtime_vars['matplotlib'][plt_ID]['num'])
        plt.show()
        plt.pause(0.001)
    elif not new_plt_id and not silent_run:
        print(" > draw plot")
        #plt.figure(runtime_vars['matplotlib'][plt_ID]['num'])
        plt.draw()
    # .. end plotting or handover
    check_path = root+os.path.sep+'TMP'
    export.check_isdir(check_path)
    tempfile = check_path+os.path.sep+'shotrun_overview.png'
    try:
        mpl.rcParams["savefig.directory"] = runtime_vars['output_dir']
        destination = runtime_vars['output_dir']+os.path.sep+func.STRIP_EXTENSION(func.GIVE_FILENAME(input_stream['filename']))+'_IOSSR.png'
        plt.savefig(destination)
        #if 'win' not in sys.platform:
        #    os.popen('cp '+destination+' '+tempfile) 
        #else:
        #    os.popen('copy '+destination+' '+tempfile)
        print(" > plot ok")
        plt.savefig(tempfile, dpi=72)
    except:
        print(" > attempt to save figure in output directory failed")
        plt.savefig(tempfile, dpi=72)
   
    # fig.canvas.mpl_disconnect(cre)
    # fig.canvas.mpl_disconnect(cid)
    
    # exit is applicable
    if silent_run:
        print(" > exit ok")
        plt.close(plt_num)
        
    return True
    
# browse latest file
def find_last(**kwargs):
    # GLOBALS
    global input_dict

    # search for defaults
    runtime_vars = kwargs.get('runtime_vars',{})
    silent_run = kwargs.get('silent_run',False)
    noplot_run = kwargs.get('noplot_run',False)
    matplotlib_id = kwargs.get('plt_ID',None)
    matplotlib_type = kwargs.get('plt_type',runtime_vars['plot_type'])
    
    #RUN
    # find latest element in working directory
    list_of_files = [os.path.join(runtime_vars['working_dir'], fname) for fname in os.listdir(runtime_vars['working_dir'])]
    latest_file = max(list_of_files, key=os.path.getmtime)
    filename=latest_file
    
    # call data analysis
    return explorador_archivos(data_file = filename, silent_run = silent_run, noplot_run = noplot_run, runtime_vars = runtime_vars, plt_ID = matplotlib_id, plt_type = matplotlib_type)
    
# browse multiple data files, get data, parse data to physics and display analysis results
def explorador_batch(**kwargs):
    # GLOBALS
    global input_dict
    
    # search for defaults
    runtime_vars = kwargs.get('runtime_vars',{})
    silent_run = kwargs.get('silent_run',False)
    noplot_run = kwargs.get('noplot_run',False)
    matplotlib_id = kwargs.get('plt_ID',None)
    matplotlib_type = kwargs.get('plt_type',runtime_vars['plot_type'])
    
    data_files = kwargs.get('data_files',None)
    
    #RUN
    # clarify files
    if data_files is None:
        data_files = select_batch(m_GUI=True)
    # process files
    input_stream = []
    if isinstance(data_files,list) or isinstance(data_files,np.ndarray):
        try:
            for file in data_files:
                exit = explorador_archivos(data_file = file, silent_run = silent_run, noplot_run = noplot_run, runtime_vars = runtime_vars, plt_ID = matplotlib_id, plt_type = matplotlib_type)
                if exit == True:
                    print('\nFINISHED ' + file)
                else:
                    print('\nERROR ON ' + file)
        except:
            print("\nMISSING ERROR HANDLER: PROBLEM DURING WORK ON FILE LIST")
    else:
        print("MISSING ERROR HANDLER: PROBLEM DURING WORK ON SINGLE FILE")

    return True
  
# online analysis
def online_analysis(**kwargs):
    global script_path
    global work_path
    # get optional input
    runtime_vars = kwargs.get('runtime_vars',{})
    action_button = kwargs.get('button',None)
    button_locklist = kwargs.get('button_locklist',[])
    matplotlib_id = kwargs.get('plt_ID',None)
    matplotlib_type = kwargs.get('plt_type',runtime_vars['plot_type'])
    noplot_run = kwargs.get('noplot_run',False)
    silent_run = kwargs.get('silent_run',False)
    # check if online analysis is already running, then abort
    #  and initiate if else
    if 'online' not in runtime_vars.keys():
        # shared bool
        runtime_vars['online'] = multiVal('i',0)
        # setval
        runtime_vars['online'].value = 1
        print(runtime_vars['online'].value)
        print('\nSTARTUP ONLINE LOOP')  
        if action_button is not None: action_button["text"] = "STOP ONLINE ANALYSIS"
    elif runtime_vars['online'].value == 1:
        runtime_vars['online'].value = 0      
        if action_button is not None: action_button["text"] = "START ONLINE ANALYSIS"   
        print(runtime_vars['online'].value)   
        print('\nCLOSE ONLINE LOOP')        
    else:
        runtime_vars['online'].value = 1
        if action_button is not None: action_button["text"] = "STOP ONLINE ANALYSIS"
        print(runtime_vars['online'].value)
        print('\nRESTART ONLINE LOOP')
    # lock risky options
    for locked_button in button_locklist:
        if 'online' in runtime_vars.keys() and runtime_vars['online'].value == 1:
            locked_button['state'] = tk.DISABLED
        else:
            locked_button['state'] = tk.NORMAL
    # exit if rest is pointless or verify the directories
    if 'online' in runtime_vars.keys() and runtime_vars['online'].value == 0:
        return True
    else:
        # verify paths
        pathval = ""
        pathval = tk.filedialog.askdirectory(title="Verify path for sniffing of data",\
                                            initialdir=runtime_vars['working_dir'], parent=None)
        if pathval != "":
            runtime_vars['working_dir'] = pathval
        
        pathval = tk.filedialog.askdirectory(title="Verify path for output of scripts",\
                                            initialdir=runtime_vars['output_dir'], parent=None)
        if pathval != "":
            runtime_vars['output_dir'] = pathval
        del pathval
    # main online loop
    def parallel_loop(go_on,runtime_vars,*args,**kwargs):
        nonlocal noplot_run
        nonlocal silent_run
        nonlocal matplotlib_type
        old_data = load.parse_datafiles(runtime_vars['working_dir'],[script_path,work_path])
        input_data = []
        while go_on.value == 1:
            # get new data files
            input_data,old_data = load.next_datafiles(runtime_vars['working_dir'],[script_path,work_path],old_data)
            # message if any
            if len(input_data) > 0:
                print('\nCALL ONLINE LOOP')
            # process data files if any
            for file in input_data:
                # call analysis procedure
                if image.ISIMG(file):
                    print(" > found new image")
                    #fig = plt.figure(figsize=(15,10), dpi=100)
                    #gs = fig.add_gridspec(2,2) #(3,2)
                    #ax0 = fig.add_subplot(gs[:, 0])
                    #ax1 = fig.add_subplot(gs[0, 1])
                    #ax2 = fig.add_subplot(gs[1, 1])
                    exit_status = explorador_archivos(data_file = file, runtime_vars = runtime_vars, noplot_run = noplot_run, plt_type = matplotlib_type, silent_run = silent_run)
                else:
                    print(" > found no image")
                    exit_status = False
                # case of no good data
                if exit_status == False:
                    # feedback
                    print('\nONLINE LOOP RETURNS FALSE')
                    # remember file
                    old_data.append(file)
                    # go to next
                    continue
                # call output procedures
                #write_out(img, silent = True)
                # remember file
                old_data.append(file)    
            # wait and listen one second
            time.sleep(1)
        return True
    
    feedback = multiVal('i',0)
    p = multiProc(target=parallel_loop, args=(runtime_vars['online'],runtime_vars))
    p.start()
    
    # exit
    return True
    
# MAIN MENUE ITEMS AND MAIN WINDOW
def run_GUI():
    # GLOBALS
    global runtime_vars
    # Create the root window 
    window = tk.Tk() 

    # Set the running ID number
    shot=IntVar()
    shot.set(1)
    
    # helper functions
    def inc_shot():
        nonlocal shot
        value=shot.get()+1
        shot.set(value)
        #print(shot.get())

    def dec_shot():
        nonlocal shot
        value=shot.get()-1
        shot.set(value)
        #print(shot.get())
    
    # set window title 
    window.title("MAIN MENU")
       
    # set window size via trim("int(width) x int(height) + int(xposition) + int(yposition)")
    wxh = func.screensize()
    screen_width = int(wxh.split('x')[0])
    screen_height = int(wxh.split('x')[1])
    scale = 3
    geometry_string = str(int(screen_width/scale))+"x"+str(int(screen_height/scale))+\
                       "+"+str(int(screen_width/3))+"+"+str(int(screen_height/3))
    window.geometry(geometry_string)
       
    # set window background
    window.config(background = "black")
    
    # manifest logo
    background_image = image.IMREAD(root+os.path.sep+"MEDIA"+os.path.sep+"IMG"+os.path.sep+"CLPU_SF.png")
    bg_width = background_image.shape[1]
    bg_height = background_image.shape[0]
    bg_proportion = 3
    
    bg_scale_width = bg_width / (screen_width/scale) * bg_proportion
    bg_scale_height = bg_height / (screen_height/scale) * bg_proportion
    bg_scale = int(max(bg_scale_height,bg_scale_width))+1 # .. '+1' as primitive floor operation
    background_image = background_image[::bg_scale,::bg_scale,:]
    
    background_image = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(background_image))
    canvas = tk.Canvas(window,width = bg_width/bg_scale, height = bg_height/bg_scale)
    canvas.create_image(0,0,image=background_image, anchor="nw")
       
    # create labels
    label_file_explorer = Label(window,  
                                text = "THOMSON PARABOLA SPECTRA ANALYZER", 
                                width = 50, height = 4,  
                                fg = "lightgreen",
                                background="black") 

    label_shot=Label(window, 
                      textvariable=shot,
                      width = 6, height = 1,  
                      fg = "lightgreen",
                      background="black")

    label_actualshot=Label(window,
                     text=("ACTUAL SHOT NUMBER:"),
                     width = 20, height = 1,  
                     fg="red",
                     background="black")
                     
    label_comment=Label(window,
                     text=("COMMENT:"),
                     width = 20, height = 1,  
                     fg="red",
                     background="black")
                     
    label_value=Label(window,
                     text=("VALUE OF INTEREST:"),
                     width = 20, height = 1,  
                     fg="red",
                     background="black")
    
    # informative text and input
    entry_comment=Entry(window)
    entry_value=Entry(window)
    
    # radiobutton options
    # survey_on_how_to_plot_display = StringVar()
    # survey_on_how_to_plot_display.set("notonewplot") # initialize
    #
    # survey_on_how_to_plot_silence = StringVar()
    # survey_on_how_to_plot_silence.set("nosilentrun") # initialize
    #
    # def plotter_survey():
    #     nonlocal survey_on_how_to_plot_display
    #     nonlocal survey_on_how_to_plot_silence
    #     return {'tonewplot':survey_on_how_to_plot_display.get(),'silentrun':survey_on_how_to_plot_silence.get()}
    #
    # radio_tonewplot   = Radiobutton(window, text="to new plot",  variable=survey_on_how_to_plot_display, value="tonewplot", command=plotter_survey)
    # radio_notonewplot = Radiobutton(window, text="overwrite plot",  variable=survey_on_how_to_plot_display, value="notonewplot", command=plotter_survey)
    # radio_silentrun   = Radiobutton(window, text="run silently", variable=survey_on_how_to_plot_silence, value="silentrun", command=plotter_survey)
    # radio_nosilentrun = Radiobutton(window, text="run display", variable=survey_on_how_to_plot_silence, value="nosilentrun", command=plotter_survey)
    # radio_logplot     = Radiobutton(window, text="log y-axis",   variable=survey_on_how_to_plot, value="logplot",   command=plotter_survey)
    
    # checkboxes for options
    def process_check(keyword):
        if keyword == 'tonewplot':
            if i_newplot.get() == True:
                return True
            else:
                return False
        elif keyword == 'plottype':
            if i_plottype.get() == True:
                return True
            else:
                return False
        elif keyword == 'silentrun':
            if i_silence.get() == True:
                return True
            else:
                return False
        else:
            return False
    
    i_plottype = BooleanVar()
    if runtime_vars['plot_type'] == 'log':
        i_plottype.set(True)
    else:
        i_plottype.set(False)
    check_plottype = Checkbutton(window, text = "log-plot", variable=i_plottype, anchor="w")
    
    i_silence = BooleanVar()
    i_silence.set(False)
    check_silence = Checkbutton(window, text = "silent run", variable=i_silence, anchor="w")
    
    i_newplot = BooleanVar()
    i_newplot.set(False)
    check_newplot = Checkbutton(window, text = "to new plot", variable=i_newplot, anchor="w")
    
    # menue items for options
    def goto_settings():
        window.destroy()
        interactive_input(exit_call = run_GUI, runtime_vars = runtime_vars)
        return True
    
    button_goto_settings =   Button(\
                               window,  \
                               text = "SETTINGS", \
                               command = goto_settings\
                             )
    
    # helper functions for interactive data analysis
    def get_pltID():
        if 'matplotlib' in runtime_vars.keys() and process_check('tonewplot') == False:
            return list(runtime_vars['matplotlib'].keys())[-1] 
        else:
            return None
            
    def get_pltTYPE():
        if process_check('plottype') == False:
            return 'linear' 
        else:
            return 'log'
            
    def get_pltEXPRESSION():
        if process_check('silentrun') == False:
            return False
        else:
            return True
    
    # inactive buttons for high level actions triggered by buttons are only possible after a reference is processed
    #  thus the trigger buttons are disabled by default
    button_explore=Button(window,  
                          text = "BROWSE FILE",
                          state=tk.DISABLED,
                          command = lambda : explorador_archivos(plt_ID = get_pltID(), plt_type = get_pltTYPE(), runtime_vars = runtime_vars, silent_run = get_pltEXPRESSION()))  
    button_proclast=Button(window,
                           text="PROCESS LATEST FILE",
                           state=tk.DISABLED,
                           command= lambda: find_last(plt_ID = get_pltID(), plt_type = get_pltTYPE(), runtime_vars = runtime_vars, silent_run = get_pltEXPRESSION()))
    button_batch   =Button(window,
                           text="PROCESS BATCH",
                           state=tk.DISABLED,
                           command= lambda: explorador_batch(plt_ID = get_pltID(), plt_type = get_pltTYPE(), runtime_vars = runtime_vars, silent_run = get_pltEXPRESSION()))
    button_online = Button(window, 
                          text="START ONLINE ANALYSIS",
                          state=tk.DISABLED,
                          command=lambda: online_analysis(button = button_online, plt_ID = get_pltID(), plt_type = get_pltTYPE(), runtime_vars = runtime_vars, button_locklist = [button_reference, button_explore, button_proclast, check_plottype, check_newplot, button_goto_settings], silent_run = get_pltEXPRESSION()))
    
    # processor functions for basic functionalities
    def saving(**kwargs):
        global runtime_vars
        nonlocal shot
        nonlocal entry_comment
        nonlocal entry_value
        # save to file
        print(" > save routine not operational")
        print(int(shot.get()))
        print(entry_comment.get())
        print(int(entry_value.get()))
        value=shot.get()+1
        shot.set(value)
        return False
        
    def unblocking(**kwargs):
        global runtime_vars
        nonlocal button_explore
        nonlocal button_proclast
        nonlocal button_batch
        nonlocal button_online
        # deblock functionalities
        if 'canvas' in runtime_vars.keys() and 'center' in runtime_vars.keys():
            print(' > unblock further options')
            button_explore['state']  = tk.NORMAL
            button_proclast['state'] = tk.NORMAL
            button_batch['state']    = tk.NORMAL
            button_online['state']   = tk.NORMAL
        else:
            print(' > canvas or centre not proper')
        return True
            
    def callback_reference(**kwargs):
        print('\nCALLBACK REFERENCE')
        print(' > browse reference')
        explorador_referencia()
        print(' > unblock buttons')
        unblocking()
        print('EXIT CALLBACK REFERENCE')
        return True
        
    def modify_directories(keyword):
        global runtime_vars
        pathval = ""
        if keyword == 'working_dir':
            pathval = tk.filedialog.askdirectory(title="Verify path for sniffing of data",\
                                            initialdir=runtime_vars['working_dir'], parent=None)
        elif keyword == 'output_dir':
            pathval = tk.filedialog.askdirectory(title="Verify path for output of scripts",\
                                            initialdir=runtime_vars['output_dir'], parent=None)
        else:
            return False
        if pathval != "" and keyword == 'working_dir':
            runtime_vars['working_dir'] = pathval
        elif pathval != "" and keyword == 'output_dir':
            runtime_vars['output_dir'] = pathval
        else:
            return False
        return True

    # create basic buttons and labels for basic functionalities
    # the reference button and primitive buttons are enabled from the beginning  
    button_reference=Button(window,  
                           text = "BROWSE REFERENCE", 
                           command = callback_reference)
    button_incshot= Button(window, 
                           text="INCREASE SHOT NUMBER +1",
                           command = inc_shot)
    button_decshot= Button(window, 
                           text="DECREASE SHOT NUMBER -1",
                           command = dec_shot)
    button_save=    Button(window, 
                           text="SAVE AND CONTINUE",
                           state=tk.DISABLED,
                           command=saving)
    button_workingdir = Button(window, 
                           text="CHANGE WORKING DIR",
                           command=lambda: modify_directories('working_dir'))   
    button_outputdir = Button(window, 
                           text="CHANGE OUTPUT DIR",
                           command=lambda: modify_directories('output_dir'))                              
                          
    # positioning
    canvas.grid(column = 1, row = 1)
    
    label_file_explorer.grid(column = 2, row = 1, columnspan = 2) 

    button_reference.grid(column = 2, row = 3, sticky="news") 
    button_explore.grid(column = 2, row = 4, sticky="news")
    
    button_goto_settings.grid(column = 3, row = 3, padx = 10, sticky = "news")
    check_newplot.grid(column = 3, row = 4, padx = 10, sticky = "news")
    check_silence.grid(column = 3, row = 5, padx = 10, sticky = "news")
    check_plottype.grid(column = 3, row = 6, padx = 10, sticky = "news")
    
    button_proclast.grid(column=2, row=5, sticky="news")
    
    button_batch.grid(column=2, row=6, sticky="news")

    button_online.grid(column=2, row=7, sticky="news")

    label_actualshot.grid(column=1, row=8)
    label_shot.grid(column =1, row= 9)

    label_comment.grid(column=2, row=8)
    entry_comment.grid(column=2, row=9)
    
    label_value.grid(column=2, row=10)
    entry_value.grid(column=2, row=11)

    button_save.grid(column=3, row=9, sticky="news")
    button_workingdir.grid(column=3, row=10, sticky="news")
    button_outputdir.grid(column=3, row=11, sticky="news")    

    button_incshot.grid(column=1, row=10)
    button_decshot.grid(column=1, row=11)

    # Let the window wait for any events 
    window.mainloop() 

# RUN WITH MODIFIED RUNTIME PARAMETERS
if globals()["__name__"] == '__main__':
    # STARTUP
    # interactive modifications
    print("\nGUI STARTUP")
    # call interactive start window
    interactive_start()
    
    # DISPERSION RELATION
    # loading dispertion relation specified in settings as 'disrel', where
    #  the input file is formatted in two columns (energy in [MeV] | vertical position in [m])
    #  and delete leading (0,0)
    #  and format array in [[energies],[positions]]
    runtime_vars['disrel'] = np.loadtxt(input_dict['disrel'])[1:].T
    # fetch spatial calibration
    if 'pixel_per_mm_dialogue' in input_dict.keys() and False:
        if input_dict['pixel_per_mm_dialogue'] == 'T':
            try:
                runtime_vars['pixel_per_mm'],err = spatialcal.get()
            except:
                runtime_vars['pixel_per_mm'] = input_dict['pixel_per_mm']
                func.MESSAGE_PRINT(globals()['__file__'],"could not boot the online calibration,","fall back to input parameter")
        else:
            runtime_vars['pixel_per_mm'] = input_dict['pixel_per_mm']
    else:
        runtime_vars['pixel_per_mm'] = input_dict['pixel_per_mm']
    
    # converting disprelprot to [energy, pixel position] matrix
    runtime_vars['disrel_pixed'] = np.array([runtime_vars['disrel'][0],\
                                (runtime_vars['disrel'][1]*const.m_to_mm*runtime_vars['pixel_per_mm'])])
                                
    # MATH
    # creating differential correction list [energy,dx_to_de] !TODO explain better what happens here
    #dxde = np.array([runtime_vars['disrel'][1,:],np.gradient(runtime_vars['disrel'][1,:],runtime_vars['disrel'][0,:])])
    disprelprot = runtime_vars['disrel'].T
    dxde=np.array([[0, 0]])
    for x in range(0, len(disprelprot)-1):
       dxde=np.append(dxde, [[(disprelprot[x, 1]-disprelprot[x+1, 1])/(disprelprot[x+1,0]-disprelprot[x,0]),disprelprot[x, 0]]],axis=0)
    dxde = dxde.T

    # !TODO and disrelprot does not get to the Xray ref via centering?

    # interpolating functions
    # .. pixel calibration function (introducing the energy gives you the vertical position in pixels)
    y_of_energy = interpolate.interp1d(runtime_vars['disrel_pixed'][0], runtime_vars['disrel_pixed'][1], fill_value="extrapolate")
    # .. inverse pixel calibration function (introducing the vertical position in pixels gives you the energy)
    energy_of_y = interpolate.interp1d(runtime_vars['disrel_pixed'][1], runtime_vars['disrel_pixed'][0], fill_value="extrapolate")
    # .. variable change function (introducing the energy gives you -dx/de) !TODO dx/dE or -dx/dE?
    runtime_vars['dxde_of_energy'] = interpolate.interp1d(dxde[1], dxde[0], fill_value="extrapolate")
    # .. define delimiters: first pixel and last pixel of vertical range covered by disperion relation
    #     and a range accordingly,
    #     calculate the respective gorizontal values
    #     and energy values
    runtime_vars['pixend']   = int(runtime_vars['disrel_pixed'][1,  0])
    runtime_vars['pixstart'] = int(runtime_vars['disrel_pixed'][1, -1])
    runtime_vars['ypixels']  = np.arange(runtime_vars['pixstart'], runtime_vars['pixend'])
    # TODO
    # RuntimeWarning: divide by zero encountered in true_divideRuntimeWarning: divide by zero encountered in true_divide
    runtime_vars['energies'] = energy_of_y(runtime_vars['ypixels'])
    # END TODO
    # GUI
    if input_dict['GUI'] == 'T' :
        run_GUI()
    else:
        func.MESSAGE_PRINT(glob_dict['__file__'],"There is no alternative to a GUI run currently. \nSet the parameter GUI in the input file to T and \nrelaunch the application.")

# ==============================================================================
# TERMINATE
# ==============================================================================
# TAKE TIME
fin_time = time.time()

# PROMPT RUNTIME
print("\nRUNTIME = %3.1f s" % (fin_time-debut))

# SAY GOODBYE
func.GOODBYE()