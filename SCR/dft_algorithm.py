# -*- coding: utf-8 -*-
"""
This is the spatial calibration module. Please do only add but not delete content.
The file can be used as stand-alone script.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload

import os
import sys

import numpy as np
import matplotlib.image as mpimg

from scipy.signal import savgol_filter

# EXTEND PATH
sys.path.append(os.path.abspath('..'+os.path.sep+'LIB'))         # !! add potential library directory
sys.path.append(os.path.abspath('..'+os.path.sep+'SCR'))         # !! add potential script directory

# INTERNAL
import functions as func        # !! global formats from SCR/functions.py
import image as image           # !! global formats from SCR/image.py
import exporter as export       # !! global formats from SCR/exporter.py

# RELOAD
reload(func)
reload(image),
reload(export)

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================

# ==============================================================================
# FUNCTIONS TO MANAGE INTERACTIVE SPATIAL CALIBRATION
# ==============================================================================

def find_FILE(**kwargs):
    # optional input
    filename = kwargs.get('filename',None)
    working_dir = kwargs.get('working_dir',os.path.expanduser('~'))
    # validate adresse
    try:
        export.check_isdir(working_dir)
        func.MESSAGE_PRINT(globals()['__file__'],"target directory exists","work in "+working_dir)
    except:
        func.MESSAGE_PRINT(globals()['__file__'],"target directory causes problems:",working_dir)
        return False
    # build addresse
    if isinstance(filename,str):
        return working_dir+os.path.sep+filename
    else:
        try:
            import tkinter as tk
            pathname =  tk.filedialog.askopenfilename(\
                            initialdir = working_dir,\
                            title = "Select a File",\
                            parent = None \
                        )
        except:
            pathname = input(" > type rel/path/to/dir/filename.type and confirm with ENTER: ")
    if isinstance(pathname,str):
        return pathname
    else:
        find_FILE(working_dir = working_dir)

def find_ROI(filename):
    # FEEDBACK
    func.MESSAGE_PRINT(globals()['__file__'],"target file is "+filename)
    # MASK SETTINGS
    mode = 'RECTANGULAR'
    # LOAD FILE
    if image.ISIMG(filename):
        img = image.IMREAD(filename)
        cropped = image.INTERACTIVE_CROP(img, adjust=True, mode=mode)
    else:
        img = None
        func.MESSAGE_PRINT(globals()['__file__'],"target file is not recognized as image fiule format")
        cropped = {'mode':mode,'corner_x':None,'corner_y':None,'width':None,'height':None} 
    
    return {'filename' : filename, 'image' : img[cropped['corner_y']:cropped['corner_y']+cropped['height'],cropped['corner_x']:cropped['corner_x']+cropped['width']],\
            'canvas' : cropped}
    
def get_FLOAT(**kwargs):
    message = kwargs.get('message',"type number")
    try:
        try:
            import tkinter as tk
        except:
            func.MESSAGE_PRINT(globals()['__file__'],"could not import tkinter")
        
        number = tk.simpledialog.askstring("Input", message,\
                                        parent=None)
    except:
        number = input(" > " + message + " and confirm with ENTER (& q > ENTER to quit): ")
    try:
        return float(number)
    except:
        if number == 'c' or number == 'q':
            return None
        else:
            return get_FLOAT()
        
def get_PIXPERMM(**kwargs):
    crop_image = kwargs.get('img',None)
    patt = kwargs.get('period',None)
    #obtaining a 1D list by averaging horizontally
    i=0
    nlist=np.average(crop_image, axis=1)
        
    #import matplotlib.pyplot as plt
    #
    #plt.plot(nlist)
    #plt.show()

    #calculating power spectrum DFT
    dft=np.fft.fft(nlist)
    power=(dft.real)**2
    #frequency step in DFT list
    fstep=1/len(crop_image)
    
    # smooth FFT for evaluation of ROI around first off-zero peak
    power_filtered = savgol_filter(power, 5, 3) # window size 10, polynomial order 3 !TODO make an option
    local_minima = np.where(np.r_[True, power_filtered[1:] < power_filtered[:-1]] & np.r_[power_filtered[:-1] < power_filtered[1:], True])
    
    # eliminating low frequencies and finding 1st harmonic position under assumption that 1st harmonic is strongest
    peak  = np.amax(power[local_minima[0][0]:local_minima[0][-1]])
    index = np.where(power==peak)[0][0] # !TODO might be fragile if selected ROI has other important spatial frequencies, we should waren the user
    
    # find HWHM
    lower_minimum_indexindex = np.where(local_minima[0] < index)[0][-1]
    upper_minimum_indexindex = np.where(local_minima[0] > index)[0][0]
    lower_minimum = local_minima[0][lower_minimum_indexindex]
    upper_minimum = local_minima[0][upper_minimum_indexindex]
    clip = power[lower_minimum:upper_minimum+1]
    clip_minus_half_max = clip - peak/2.
    tip_of_the_iceberg = np.where(clip_minus_half_max > 0)[0]
    lower_boundary = tip_of_the_iceberg[0] + lower_minimum
    if len(tip_of_the_iceberg) > 1:
        upper_boundary = tip_of_the_iceberg[-1] + upper_minimum
    else:
        upper_boundary = tip_of_the_iceberg[0] + upper_minimum

    # converting index of list into freqency and then period
    period=1./(index*fstep)
    
    # convert FWHM of spectrum around 1st harmonic into guess for space uncertainty
    uncert = 1./(lower_boundary*fstep) - 1./(upper_boundary*fstep)

    # if calculated period corresponds to pattern size, then one millimeter is...
    pix_ratio=period/patt
    pix_err  =uncert/patt

    #print pixel per mm ratio
    return pix_ratio,pix_err

# ==============================================================================
# MAIN
# ==============================================================================

def get():
    # fetch input file
    file = find_FILE()
    # update working dir
    #working_dir = func.GIVE_DIRNAME(file)
    
    # Determining the ROI for pattern analysis -> 'canvas'
    #  importing and cropping image (example)  -> 'img'
    ROI = find_ROI(file)
    crop_image = ROI['image']

    # calibration pattern real size in mm (min-to-min distance)
    patt = get_FLOAT(message = "seek calibration pattern real size in mm (mix-to-min distance)\n   type number (int or float)")

    # OLD CODE
    # importing and cropping image (example)
    #img=mpimg.imread("C:\\Users\\japinaniz\\Desktop\\alessio processing\\MAGNIFICATION REFERENCES\\reference ruler filtered.tif")   
    #crop_image=img[cropcoords[0, 0]:cropcoords[0, 1], cropcoords[1, 0]:cropcoords[1, 1]]
    # END OLD CODE
    
    # DFT for pix per mm
    return get_PIXPERMM(img = crop_image, period = patt)

if globals()["__name__"] == '__main__':
    print('\nBOOT SPATIAL CALIBRATION TOOL')
    pix_ratio = get()
    print("\nthe ratio is"+" "+str(pix_ratio)+" "+"pixels per mm")


