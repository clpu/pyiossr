"""
CATCHES FILES THAT ARE AUTOMATICALLY SAVED TO A DIRECTORY AND 
RENAMES THEM WITH PREFIX AND INCREASING NUMBER.

flags {
  -d --directory   is input directory (absolute path)     expect string
  -p --nprefix     is prefix (name base)                  expect string
  -n --number      is first number                        expect integer
}


execute command: python catch_vimba_auto_rename.py *args

"""
# ==============================================================================
# IMPORT PYTHON MODULES
# ==============================================================================
import os                                       # !! methods of operating system
import sys                                      # !! executions by operating system
import time


# ==============================================================================
# SET UP RUNTIME
# ==============================================================================
description = 'Script waits for new files in directory and renames them to\n prefix_number.extension - without changing the extension.'

# PROMPT GREETINGS
print('\n WELCOME')

print('\n ' + description + '\n EXIT with CTRL + C !')

# VARIABLES AND PARAMETERS

run_lib = {}


# ==============================================================================
# LOAD COMMAND LINE
# ==============================================================================
# ARGPARSER
import argparse
parser = argparse.ArgumentParser(description=description)

parser.add_argument("-d", "--directory", help="input directory (absolute path)", default=os.getcwd())
parser.add_argument("-p", "--prefix",    help="prefix (name base)",              required=True)
parser.add_argument("-n", "--number",    help="first number",                    required=True)

run_lib['cmd'] = parser.parse_args()

del argparse, parser

# ==============================================================================
# RUN
# ==============================================================================

os.chdir(run_lib['cmd'].directory)

path = os.getcwd()

run_lib['old_files'] = []
file_list = os.listdir(path)
for file in file_list:
    run_lib['old_files'].append(file)

check = True

n = int(run_lib['cmd'].number)

while check:
    time.sleep(1.0)
    file_list = os.listdir(path)
    for file in file_list:
        if file in run_lib['old_files']:
            continue
        else:
            #run_lib['old_files'].append(file)
            
            filename_and_extension = file.rsplit( ".", 1 )
            
            try:
                extension = filename_and_extension[1]
            except:
                extension = ""
            
            new_file = run_lib['cmd'].prefix+str(n).zfill(3)+'.'+extension
            run_lib['old_files'].append(new_file)
            
            n = n+1
            
            old_name = os.path.join(path,file)
            new_name = os.path.join(path,new_file)
            
            if os.path.isfile(new_name):
                print(" FILE ALREADY EXISTS")
                check = False
                break
            else:
                # Rename the file
                if file == file_list[-1]:
                    time.sleep(0.5)
                os.rename(old_name, new_name)
            